#!/bin/bash

HELPER_TEXT="   ./create_package.sh <app-name> <deb-version> <arch[arm, arm64, amd64(default)]>"

if [[ -z "$1" ]]; then
   echo "Empty name $1. Please fill..."
   echo "Sample usage:"
   echo "$HELPER_TEXT"
   exit 1
fi

if [[ -z "$2" ]]; then
   echo "Empty version $2. Please fill..."
   echo "Sample usage:"
   echo "$HELPER_TEXT"
   exit 1
fi

# Get deb package template
DEB_FILE=DEBIAN_FILES.tar.gz
if [[ -f "$DEB_FILE" ]]; then
    echo "$DEB_FILE exist"
else
    wget https://gitlab.com/ozogulf/ci-files/raw/master/paketleme/DEBIAN_FILES.tar.gz
fi
tar -xvf DEBIAN_FILES.tar.gz

# Get prepare
PREPARE_FILE=prepare-deb.sh
if [[ -f "$PREPARE_FILE" ]]; then
    echo "$PREPARE_FILE exist"
else
    wget https://gitlab.com/ozogulf/ci-files/raw/master/paketleme/prepare-deb.sh
fi
chmod +x prepare-deb.sh

if [[ "$3" == "arm" ]]; then
    echo "build for arm"
    DOTNET_BUILD_RID=linux-arm
elif [[ "$3" == "arm64" ]]; then
    echo "build for arm64"
    DOTNET_BUILD_RID=linux-arm64
else
    echo "build for amd64"
    DOTNET_BUILD_RID=linux-x64
fi

# Create deb folder
./prepare-deb.sh -a $1 -u ieg -e iegursoy28@gmail.com -v "$2"

TARGET_FOLDER="$1_$2"

# Put app files
APP_DES_FOLDER="$TARGET_FOLDER/usr/local/bin/upnp/"

mkdir -p "$APP_DES_FOLDER"
dotnet publish -c Release -r "$DOTNET_BUILD_RID" -o "$APP_DES_FOLDER" -p:PublishSingleFile=true -p:PublishTrimmed=true  service/service.csproj --self-contained

# Put services
SERVICES_DES_FOLDER="$TARGET_FOLDER/etc/systemd/system/"

mkdir -p "$SERVICES_DES_FOLDER"
cp deb_services/*.service "$SERVICES_DES_FOLDER"

# Put post installation sh
cp deb_services/postinst "$TARGET_FOLDER/DEBIAN/"

# Create deb
dpkg-deb -b "$TARGET_FOLDER"

# Rename deb
cp "$TARGET_FOLDER.deb" "$1_$3_$2.deb"