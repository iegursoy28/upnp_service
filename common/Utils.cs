using System;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;

namespace Common
{
    public class Utils
    {
        public static string GetLocalIPAddress()
        {
            try
            {
                if (!System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
                    throw new Exception("No network adapters with an IPv4 address in the system!");

                foreach (var item in System.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces())
                {
                    System.Console.WriteLine($"Try binding network interface name: {item.Name}");
                    if (item.OperationalStatus != System.Net.NetworkInformation.OperationalStatus.Up)
                        continue;

                    if (item.GetIPProperties()?.GatewayAddresses?.Count <= 0)
                        continue;

                    foreach (var ip in item.GetIPProperties().UnicastAddresses)
                    {
                        if (ip.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                            return ip.Address.ToString();
                    }
                }

                throw new Exception("No network adapters with an IPv4 address in the system!");
            }
            catch (Exception e)
            {
                System.Console.WriteLine(e);
            }

            return null;
        }

        public static OSPlatform GetOperatingSystem()
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
            {
                return OSPlatform.OSX;
            }

            if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                return OSPlatform.Linux;
            }

            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                return OSPlatform.Windows;
            }

            throw new Exception("Cannot determine operating system!");
        }
    }
}