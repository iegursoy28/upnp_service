using System;
using System.Net;
using System.Text;

namespace Common
{
    public class HTTPServerManager
    {
        public static void Start(string rootPath, string archivablePath, int port = 7000)
        {
            var dir = rootPath ??= Environment.CurrentDirectory;
            Westwind.WebConnection.SimpleHttpServer.StartHttpServerOnThread(dir, archivablePath, port);
            System.Console.WriteLine($"Set archivable path: {archivablePath}");
            System.Console.WriteLine($"HTTP server started 0.0.0.0:{port}\npath: {dir}");
        }

        public static void Stop()
        {
            Westwind.WebConnection.SimpleHttpServer.StopHttpServerOnThread();
            System.Console.WriteLine("HTTP server stopped");
        }
    }
}