using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using CommandLine;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Hosting;
using LibIEG;

namespace service
{
    public class Program
    {
        public static Logger logger = new Logger("upnp_service") { IsLog = false };

        class Options
        {
            [Option('h', "http_port", Default = 9080, HelpText = "HTTP server port")]
            public int HTTP_PORT { get; set; }

            [Option('g', "grpc_port", Default = 9051, HelpText = "gRPC server port")]
            public int GRPC_PORT { get; set; }

            [Option('u', "disable_upnp", Default = false, HelpText = "Disable UPnP service")]
            public bool DisableUPnP { get; set; }
        }

        public static void Main(string[] args)
        {
            CommandLine.Parser.Default
            .ParseArguments<Options>(args)
            .WithParsed(RunOptions)
            .WithNotParsed(HandleParseError);
        }

        static void HandleParseError(IEnumerable<Error> errs)
        {
            foreach (var item in errs)
                logger.Warn(item.ToString());
        }

        static void RunOptions(Options opts)
        {
            #region Initialize Variables
            var localIP = Utils.GetLocalIPAddress();
            var hostname = Dns.GetHostName();
            var deviceUri = $"http://{localIP ??= "0.0.0.0"}";

            var device = new Rssdp.SsdpRootDevice()
            {
                CacheLifetime = TimeSpan.FromMinutes(30),
                Location = new Uri($"{deviceUri}:{opts.HTTP_PORT}/root_device.xml"),
                DeviceTypeNamespace = "schemas-upnp-org",
                DeviceType = "Basic",
                FriendlyName = $"{hostname ??= "RootDevice"}",
                Manufacturer = "Sparse Tech",
                ModelName = $"{Utils.GetOperatingSystem()}",
                Uuid = Guid.NewGuid().ToString(),
                Icons = {
                    new Rssdp.SsdpDeviceIcon() {
                        MimeType = "image/png",
                        Height = 48,
                        Width = 48,
                        ColorDepth = 24,
                        Url = new Uri($"/logo.png", UriKind.Relative)
                    }
                },
            };
            device.AddService(new Rssdp.SsdpService()
            {
                Uuid = Guid.NewGuid().ToString(),
                ServiceType = "GRPCService",
                ServiceTypeNamespace = "upnp_service.service",
                ControlUrl = new Uri($"{deviceUri}:{opts.GRPC_PORT}"),
                ScpdUrl = new Uri($"{deviceUri}:{opts.GRPC_PORT}"),
            });
            #endregion

            // Start HTTP server
            var sp = new ServerStartParameters() { path = Utils.AppBasePath, port = opts.HTTP_PORT };
            SimpleHttpServer.StartHttpServerOnThread(new CustomHttpServer(sp));

            // Start UPNP service
            if (!opts.DisableUPnP)
                UPNPServiceManager.GetInstance().AddDevice(device);

            // Start GRPC server
            CreateHostBuilder(opts.GRPC_PORT).Build().Run();

            // Stop upnp
            UPNPServiceManager.GetInstance().Stop();

            // Stop HTTP server
            SimpleHttpServer.StopHttpServerOnThread();
        }

        // Additional configuration is required to successfully run gRPC on macOS.
        // For instructions on how to configure Kestrel and gRPC clients on macOS, visit https://go.microsoft.com/fwlink/?linkid=2099682
        public static IHostBuilder CreateHostBuilder(int grpcPort) =>
            Host.CreateDefaultBuilder()
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseKestrel();
                    webBuilder.ConfigureKestrel((context, options) =>
                    {
                        options.ListenAnyIP(grpcPort, listenOptions => { listenOptions.Protocols = HttpProtocols.Http2; });
                        //options.ConfigureEndpointDefaults(lo => lo.Protocols = HttpProtocols.Http2);
                    });
                    webBuilder.UseStartup<Startup>();
                });
    }
}
