using System;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Threading;
using LibIEG;

namespace service
{
    public class CustomHttpServer : SimpleHttpServer
    {
        public CustomHttpServer(ServerStartParameters parameters) : base(parameters)
        { }

        protected override void Process(HttpListenerContext context)
        {
            string ARCHIVE_PATH = Path.Combine(_serverParameters.path, ".archive_path");

            var filename = context.Request.Url.AbsolutePath.Substring(1);
            var filePath = Path.Combine(_serverParameters.path, filename);

            Console.WriteLine($"Request file: {filename}");

            if (!File.Exists(ARCHIVE_PATH))
                File.WriteAllText(ARCHIVE_PATH, Utils.AppBasePath);
            var _archivePath = File.ReadAllText(ARCHIVE_PATH);

            if (filePath.EndsWith(".h264"))
                filePath = Path.Combine(_archivePath, filename);

            if (filePath.EndsWith("all_zip_size"))
            {
                long total_size = 0;
                foreach (var file in System.IO.Directory.GetFiles(_archivePath))
                    total_size += new FileInfo(file).Length;

                string mime = "application/octet-stream";
                context.Response.ContentType = mime;
                context.Response.AddHeader("File-Size", $"{total_size}");
            }
            else if (filePath.EndsWith("all.zip"))
            {
                try
                {
                    if (String.IsNullOrEmpty(_archivePath) || !System.IO.Directory.Exists(_archivePath))
                        throw new Exception("Not set archivable files!!");

                    using (var archive = new ZipArchive(new TrackablePositionStream(context.Response.OutputStream), ZipArchiveMode.Create, true))
                    {
                        string mime = "application/octet-stream";
                        context.Response.ContentType = mime;
                        context.Response.AddHeader("Date", DateTime.Now.ToString("r"));
                        context.Response.AddHeader("Last-Modified", File.GetLastWriteTime(filePath).ToString("r"));

                        foreach (var item in System.IO.Directory.GetFiles(_archivePath))
                        {
                            string entryItem = item;
                            string mp4FileName = null;
                            if (entryItem.EndsWith(".h264"))
                            {
                                mp4FileName = entryItem.Replace(".h264", ".mp4");
                                using (Process p = new Process())
                                {
                                    p.StartInfo.FileName = "ffmpeg";
                                    p.StartInfo.Arguments = $"-y -i {entryItem} -c copy {mp4FileName}";
                                    p.StartInfo.UseShellExecute = false;
                                    p.StartInfo.CreateNoWindow = true;

                                    p.Start();
                                    while (!p.HasExited)
                                        Thread.Sleep(10);

                                    entryItem = mp4FileName;
                                }
                            }

                            System.Console.WriteLine(entryItem);
                            using (var fileStream = new FileStream(entryItem, FileMode.Open))
                            {
                                var en = archive.CreateEntry(entryItem, CompressionLevel.NoCompression);
                                en.ExternalAttributes = en.ExternalAttributes | (Convert.ToInt32("664", 8) << 16);
                                using (var o = en.Open())
                                {
                                    fileStream.Seek(0, SeekOrigin.Begin);
                                    fileStream.CopyTo(o);
                                }
                            }

                            if (!string.IsNullOrEmpty(mp4FileName))
                                System.IO.File.Delete(mp4FileName);
                        }
                        context.Response.OutputStream.Flush();
                        context.Response.StatusCode = (int)HttpStatusCode.OK;
                    }
                }
                catch (Exception e)
                {
                    context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    context.Response.StatusDescription = $"{e.Message}";
                    System.Console.WriteLine(e);
                }
            }
            else if (File.Exists(filePath))
            {
                try
                {
                    if (String.IsNullOrEmpty(filePath) || !System.IO.File.Exists(filePath))
                        throw new Exception($"Not found {filePath} !!");

                    if (filePath.EndsWith(".h264"))
                    {
                        var mp4FileName = filePath.Replace(".h264", ".mp4");
                        using (Process p = new Process())
                        {
                            p.StartInfo.FileName = "ffmpeg";
                            p.StartInfo.Arguments = $"-y -i {filePath} -c copy {mp4FileName}";
                            p.StartInfo.UseShellExecute = false;
                            p.StartInfo.CreateNoWindow = true;

                            p.Start();
                            while (!p.HasExited)
                                Thread.Sleep(10);

                            filePath = mp4FileName;
                        }
                    }

                    Stream input = new FileStream(filePath, FileMode.Open);

                    //Adding permanent http response headers
                    string mime;
                    context.Response.ContentType = Utils._mimeTypeMappings.TryGetValue(Path.GetExtension(filePath), out mime)
                        ? mime
                        : "application/octet-stream";
                    context.Response.ContentLength64 = input.Length;
                    context.Response.AddHeader("Content-Disposition", $"filename=\"{Path.GetFileName(filePath)}\"");
                    context.Response.AddHeader("Date", DateTime.Now.ToString("r"));
                    context.Response.AddHeader("Last-Modified", File.GetLastWriteTime(filePath).ToString("r"));

                    byte[] buffer = new byte[1024 * 32];
                    int nbytes;
                    while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                        context.Response.OutputStream.Write(buffer, 0, nbytes);
                    input.Close();
                    context.Response.OutputStream.Flush();

                    if (!string.IsNullOrEmpty(filePath) && filePath.Contains("mp4"))
                        System.IO.File.Delete(filePath);

                    context.Response.StatusCode = (int)HttpStatusCode.OK;
                }
                catch (Exception)
                {
                    context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                }
            }
            else
            {
                context.Response.StatusCode = (int)HttpStatusCode.NotFound;
            }

            context.Response.OutputStream.Close();
        }
    }
}