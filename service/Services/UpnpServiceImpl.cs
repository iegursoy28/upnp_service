using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Upnp;
using LibIEG;

namespace service
{
    public class UpnpServiceImpl : Upnp.UpnpServices.UpnpServicesBase
    {
        private readonly ILogger<UpnpServiceImpl> _logger;
        public UpnpServiceImpl(ILogger<UpnpServiceImpl> logger)
        {
            _logger = logger;
        }

        public override Task<ResponseQ> AddService(RequestAddService request, Grpc.Core.ServerCallContext context)
        {
            try
            {
                UPNPServiceManager.GetInstance().AddService(new Rssdp.SsdpService()
                {
                    Uuid = request.Uuid,
                    ServiceType = request.ServiceType,
                    ServiceTypeNamespace = request.ServiceTypeNamespace,
                    ControlUrl = new Uri(request.ControlUrl),
                    ScpdUrl = new Uri(request.ScpdUrl),
                });

                return Task.FromResult(new ResponseQ() { RespCode = (int)Grpc.Core.StatusCode.OK });
            }
            catch (Exception e)
            {
                return Task.FromResult(new ResponseQ() { RespCode = (int)Grpc.Core.StatusCode.Aborted, RespMsg = e.Message });
            }
        }

        public override Task<ResponseQ> DelService(RequestDelService request, Grpc.Core.ServerCallContext context)
        {
            try
            {
                UPNPServiceManager.GetInstance().RemoveService(request.Uuid);
                return Task.FromResult(new ResponseQ() { RespCode = (int)Grpc.Core.StatusCode.OK });
            }
            catch (Exception e)
            {
                return Task.FromResult(new ResponseQ() { RespCode = (int)Grpc.Core.StatusCode.Aborted, RespMsg = e.Message });
            }
        }
    }
}
